$(document).ready(function () {
    $('#settingFrame').hide();
    $('#newGroup').hide();
    $("#barsLeft").click(function () {
        $("#settingFrame").fadeToggle();
        $("#contactsList").slideToggle();
    })
    $("#createGroup").click(function () { 
        $('#newGroup').slideDown();
    });
});